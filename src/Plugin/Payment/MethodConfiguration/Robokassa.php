<?php
/**
 * Contains \Drupal\robokassa_payment\Plugin\Payment\MethodConfiguration\Robokassa.
 */

namespace Drupal\robokassa_payment\Plugin\Payment\MethodConfiguration;


use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationInterface;
use Drupal\payment_offsite_api\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBaseOffsite;


/**
 * Provides the configuration for the payment_robokassa payment method plugin.
 *
 * Plugins extending this class should provide a configuration schema that
 * extends
 * plugin.plugin_configuration.payment_method_configuration.payment_robokassa.
 *
 * @PaymentMethodConfiguration(
 *   id = "payment_robokassa",
 *   label = @Translation("Robokassa"),
 *   description = @Translation("A payment method type that process payments via Robokassa payment gateway.")
 * )
 */
class Robokassa extends PaymentMethodConfigurationBaseOffsite implements PaymentMethodConfigurationInterface {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'message_text' => 'In addition to the order amount robokassa fee can be charged.',
      'message_text_format' => 'plain_text',
      'ipn_statuses' => [
        'success' => 'payment_success',
        'fail' => 'payment_failed',
      ],
      'config' => [
        'MrchLogin' => '',
        'pass1' => '',
        'pass2' => '',
        'server' => 'test',
        'hash_type' => 'md5',
        'recurring' => FALSE,
      ],
    ];
  }

  /**
   * Implements a form API #process callback.
   */
  public function processBuildConfigurationForm(array &$element, FormStateInterface $form_state, array &$form) {
    $element['MrchLogin'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Login'),
      '#description' => $this->t('Your robokassa login'),
      '#default_value' => $this->getMrchLogin(),
      '#required' => TRUE,
    );

    $element['pass1'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('First password'),
      '#description' => $this->t('Password 1'),
      '#default_value' => $this->getPass1(),
      '#required' => TRUE,
    );

    $element['pass2'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Second password'),
      '#description' => $this->t('Password 2'),
      '#default_value' => $this->getPass2(),
      '#required' => TRUE,
    );

    $element['server'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Robokassa server'),
      '#options' => array(
        'test' => $this->t('Test - use for testing.'),
        'live' => $this->t('Live - use for processing real transactions'),
      ),
      '#default_value' => $this->getServer(),
      '#required' => TRUE,
    );

    $element['hash_type'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Hash type'),
      '#options' => array(
        'md5' => 'md5',
        'ripemd160' => 'ripemd160',
        'sha1' => 'sha1',
        'sha256' => 'sha256',
        'sha384' => 'sha384',
        'sha512' => 'sha512',
      ),
      '#default_value' => $this->getHashType(),
      '#required' => TRUE,
    );

    $element['recurring'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Recurring payments'),
      '#default_value' => $this->getRecurring(),
      '#required' => FALSE,
    );

    return parent::processBuildConfigurationForm($element, $form_state, $form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $parents = $form['plugin_form']['#parents'];
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    $this->setMrchLogin($values['MrchLogin']);
    $this->setPass1($values['pass1']);
    $this->setPass2($values['pass2']);
    $this->setServer($values['server']);
    $this->setHashType($values['hash_type']);
    $this->setRecurring($values['recurring']);
  }

  /**
   * Gets the MrchLogin.
   *
   * @return string
   */
  public function getMrchLogin() {
    return $this->configuration['config']['MrchLogin'];
  }

  /**
   * Sets the MrchLogin.
   *
   * @param string $mrch_login
   *
   * @return static
   */
  public function setMrchLogin($mrch_login) {
    $this->configuration['config']['MrchLogin'] = $mrch_login;

    return $this;
  }

  /**
   * Gets the pass1.
   *
   * @return string
   */
  public function getPass1() {
    return $this->configuration['config']['pass1'];
  }

  /**
   * Sets the pass1.
   *
   * @param string $pass
   *
   * @return static
   */
  public function setPass1($pass) {
    $this->configuration['config']['pass1'] = $pass;

    return $this;
  }

  /**
   * Gets the pass2.
   *
   * @return string
   */
  public function getPass2() {
    return $this->configuration['config']['pass2'];
  }

  /**
   * Sets the pass2.
   *
   * @param string $pass
   *
   * @return static
   */
  public function setPass2($pass) {
    $this->configuration['config']['pass2'] = $pass;

    return $this;
  }

  /**
   * Gets the pass2.
   *
   * @return string
   */
  public function getServer() {
    return $this->configuration['config']['server'];
  }

  /**
   * Sets the server.
   *
   * @param string $server
   *
   * @return static
   */
  public function setServer($server) {
    $this->configuration['config']['server'] = $server;

    return $this;
  }

  /**
   * Gets the pass2.
   *
   * @return string
   */
  public function getHashType() {
    return $this->configuration['config']['hash_type'];
  }

  /**
   * Sets the server.
   *
   * @param string $server
   *
   * @return static
   */
  public function setHashType($hash_type) {
    $this->configuration['config']['hash_type'] = $hash_type;

    return $this;
  }

  /**
   * Gets the recurring.
   *
   * @return bool
   */
  public function getRecurring() {
    return $this->configuration['config']['recurring'];
  }

  /**
   * Sets the recurring.
   *
   * @param bool $recurring
   *
   * @return static
   */
  public function setRecurring($recurring = FALSE) {
    $this->configuration['config']['recurring'] = $recurring;

    return $this;
  }

}
