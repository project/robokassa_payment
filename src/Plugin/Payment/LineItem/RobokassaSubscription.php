<?php

/**
 * Contains \Drupal\robokassa_payment\Plugin\Payment\LineItem\RobokassaSubscription.
 */

namespace Drupal\robokassa_payment\Plugin\Payment\LineItem;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\payment\Plugin\Payment\LineItem\Basic;

/**
 * A Robocassa subscription line item.
 *
 * Plugins extending this class should provide a configuration schema that
 * extends plugin.plugin_configuration.line_item.payment_basic.
 *
 * @PaymentLineItem(
 *   id = "payment_robokassa_subscription",
 *   label = @Translation("Robokassa Subscription")
 * )
 */
class RobokassaSubscription extends Basic {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'periods' => [],
      'expiration_date' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function cancelSubscription() {
    $this->setExpirationDate(-1);
  }

  /**
   * {@inheritdoc}
   */
  public function setExpirationDate($expiration_date) {
    $this->configuration['expiration_date'] = $expiration_date;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpirationDate() {
    return $this->configuration['expiration_date'];
  }

  /**
   * {@inheritdoc}
   */
  public function setPeriods($periods) {
    $this->configuration['periods'] = $periods;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPeriods() {
    return $this->configuration['periods'];
  }

  /**
   * Implements form #pre_render callback.
   *
   * @throws \InvalidArgumentException
   */
  public function preRender(array $element) {
    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $elements = parent::buildConfigurationForm($form, $form_state);
    $elements['periods'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Periods'),
      '#description' => $this->t('Format is "period length in seconds|human readable period name", single period per line.'),
      '#default_value' => $this->getPeriods(),
      '#required' => TRUE,
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $form['#parents']);

    $this->setPeriods($values['periods']);
  }

}
