<?php

/**
 * Contains \Drupal\robokassa_payment\Plugin\Payment\LineItem\RobokassaRegular.
 */

namespace Drupal\robokassa_payment\Plugin\Payment\LineItem;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\payment\Plugin\Payment\LineItem\Basic;

/**
 * A Robokassa regular line item.
 *
 * Plugins extending this class should provide a configuration schema that
 * extends plugin.plugin_configuration.line_item.payment_basic.
 *
 * @PaymentLineItem(
 *   id = "payment_robokassa_regular",
 *   label = @Translation("Robokassa Regular")
 * )
 */
class RobokassaRegular extends Basic {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'previous_payment' => [],
      'expiration_date' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setExpirationDate($expiration_date) {
    $this->configuration['expiration_date'] = $expiration_date;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpirationDate() {
    return $this->configuration['expiration_date'];
  }

  /**
   * {@inheritdoc}
   */
  public function setPreviousPayment($previous_payment) {
    $this->configuration['previous_payment'] = $previous_payment;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviousPayment() {
    return $this->configuration['previous_payment'];
  }

  /**
   * Implements form #pre_render callback.
   *
   * @throws \InvalidArgumentException
   */
  public function preRender(array $element) {
    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $elements = parent::buildConfigurationForm($form, $form_state);
    $elements['previous_payment'] = array(
      '#type' => 'number',
      '#title' => $this->t('Previous payment'),
      '#description' => $this->t('Previous payment ID.'),
      '#default_value' => $this->getPreviousPayment(),
      '#required' => TRUE,
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $form['#parents']);

    $this->setPreviousPayment($values['previous_payment']);
  }

}
