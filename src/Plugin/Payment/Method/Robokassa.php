<?php

/**
 * Contains \Drupal\robokassa_payment\Plugin\Payment\Method\Robokassa.
 */

namespace Drupal\robokassa_payment\Plugin\Payment\Method;


use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\payment\Entity\Payment;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteSimple;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteSimpleInterface;
use Drupal\robokassa_payment\Event\RobokassaEvents;
use Drupal\robokassa_payment\Event\RobokassaPaymentRecurringEvent;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;

/**
 * A basic payment method that does not transfer money.
 *
 * Plugins that extend this class must have the following keys in their plugin
 * definitions:
 * - entity_id: (string) The ID of the payment method entity the plugin is for.
 * - execute_status_id: (string) The ID of the payment status plugin to set at
 *   payment execution.
 *
 * @PaymentMethod(
 *   id = "payment_robokassa",
 *   deriver = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteDeriver",
 *   operations_provider = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteOperationsProvider"

 * )
 */
class Robokassa extends PaymentMethodOffsiteSimple implements PaymentMethodOffsiteSimpleInterface {

  /**
   * Robokassa server payment URL.
   */
  const SERVER_URL = 'https://auth.robokassa.ru/Merchant/Index.aspx';
  const TEST_SERVER_URL = 'https://auth.robokassa.ru/Merchant/Index.aspx';
  const RECURRING_SERVER_URL = 'https://auth.robokassa.ru/Merchant/Recurring';

  /**
   * {@inheritdoc}
   */
  public function getSupportedCurrencies() {
    return TRUE;
  }

  /**
   * Gets the ID of the payment method this plugin is for.
   *
   * @return string
   */
  public function getEntityId() {
    return $this->pluginDefinition['entity_id'];
  }

  public function getServer() {
    return $this->pluginDefinition['config']['server'];
  }

  public function getAction() {
    return $this->getServer() == 'test' ? self::TEST_SERVER_URL : self::SERVER_URL;
  }

  public function getMerchant() {
    return $this->pluginDefinition['config']['MrchLogin'];
  }

  public function getPass1() {
    return $this->pluginDefinition['config']['pass1'];
  }

  public function getPass2() {
    return $this->pluginDefinition['config']['pass2'];
  }

  public function getHashType() {
    return $this->pluginDefinition['config']['hash_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentStatusAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettablePaymentStatuses(AccountInterface $account, PaymentInterface $payment) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getResultPages() {
    return [
      'success' => FALSE,
      'fail' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecutePayment() {
    $payment = $this->getPayment();
    /** @var \Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemInterface $line_item */
    $line_items = $payment->getLineItems();
    $line_item = reset($line_items);
    if ($line_item->getPluginId() != 'payment_robokassa_regular') {
      return;
    }
    $fields_string = [
      'MrchLogin' => $this->getMerchant(),
      'InvoiceID' => $payment->id(),
      'OutSum'=> round($payment->getAmount(), 2),
      'SignatureValue' => $this->getSignature(),
      'PreviousInvoiceID' => $line_item->getConfiguration()['previous_payment'],
    ];

    // For test transactions.
    if ($this->getServer() == 'test') {
      $fields_string['IsTest'] = '1';
    }

    $response = \Drupal::httpClient()
      ->post(self::RECURRING_SERVER_URL, [
        'form_params' => $fields_string,
        'http_errors' => FALSE,
      ])->getBody()->getContents();

    $this->logger->debug($response);
  }

  /**
   * {@inheritdoc}
   */
  public function paymentForm() {
    $form = array();
    $payment = $this->getPayment();
    $form['#action'] = $this->getAction();

    $this->addPaymentFormData('MrchLogin', $this->getMerchant());
    $this->addPaymentFormData('InvId', $payment->id());
    $this->addPaymentFormData('OutSum', round($payment->getAmount(), 2));
    $this->addPaymentFormData('SignatureValue', $this->getSignature());

    // For test transactions.
    if ($this->getServer() == 'test') {
      $this->addPaymentFormData('IsTest', '1');
    }

    // For recurring payments.
    $route_name = $this->request->attributes->get(RouteObjectInterface::ROUTE_NAME);
    if ($this->isRecurring() && $route_name == 'payment.offsite.redirect') {
      $this->addPaymentFormData('Recurring', 'true');
    }
    if ($this->isRecurring() && $route_name != 'payment.offsite.redirect') {
      $this->addPaymentFormData('PreviousInvoiceID', 'true');
    }

    $form += $this->generateForm();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getSignature($signature_type = PaymentMethodOffsiteSimpleInterface::SIGN_OUT) {
    $payment = $this->getPayment();
    $amount = $signature_type == PaymentMethodOffsiteSimpleInterface::SIGN_OUT ? $payment->getAmount() : (string)$this->request->request->get($this->getAmountName());
    $payment_id = $signature_type == PaymentMethodOffsiteSimpleInterface::SIGN_OUT ? $payment->id() : (string)$this->request->request->get($this->getTransactionIdName());

    $signature_data = [];
    if ($signature_type == PaymentMethodOffsiteSimple::SIGN_OUT) {
      $signature_data[] = $this->getMerchant();
    }

    $signature_data[] = $signature_type == PaymentMethodOffsiteSimpleInterface::SIGN_OUT ? round($amount, 2) : $amount;
    $signature_data[] = $payment_id;
    $signature_data[] = $signature_type == PaymentMethodOffsiteSimpleInterface::SIGN_OUT ? $this->getPass1() : $this->getPass2();

    if ($this->isVerbose()) {
      $this->logger->error('Amount: <pre>@amount</pre> Signature type: <pre>@signature_type</pre> Signature data: <pre>@data</pre> Hash type: <pre>@hash_type</pre>',
        [
          '@signature_type' => print_r($signature_type, TRUE),
          '@amount' => print_r($amount, TRUE),
          '@data' => print_r($signature_data, TRUE),
          '@hash_type' => $this->getHashType(),
        ]
      );
    }
    // Calculate signature.
    return hash($this->getHashType(), implode(':', $signature_data));
  }

  /**
   * Return local payment status related to given remote.
   *
   * @param string $status
   *   Remote status id.
   *
   * @return string.
   *   Local status id.
   */
  public function getStatusId($status) {
    return $this->pluginDefinition['ipn_statuses'][$status];
  }

  /**
   * {@inheritdoc}
   */
  protected function getValidators() {
    return [
      'validateEmpty',
      'validateRequiredKeys',
      'validateSignature',
      'validateTransactionId',
      'validateAmount',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function ipnExecute() {
    if (!$this->ipnValidate()) {
      return [
        'status' => 'fail',
        'message' => 'bad data',
        'response_code' => 200,
      ];
    }

    $inv_id = $this->request->request->get('InvId');
    $payment_status = $this->getStatusId('success');
    $status = isset($payment_status) ? $payment_status : 'payment_pending';
    $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($status));
    $this->getPayment()->save();
    $line_items = $this->getPayment()->getLineItems();
    $recurred_line_item_ids = [
      'payment_robokassa_subscription',
      'payment_robokassa_regular',
    ];
    foreach ($line_items as $line_item) {
      if (!in_array($line_item->getPluginId(), $recurred_line_item_ids)) {
        continue;
      }
      $configuration = $line_item->getConfiguration();
      if (isset($configuration['previous_payment'])) {
        $previous_payment_id = $configuration['previous_payment'];
        $previous_payment = Payment::load($previous_payment_id);
        /** @var \Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemInterface[] $previous_line_item */
        $previous_line_items = $previous_payment->getLineItems();
        $previous_line_item = reset($previous_line_items);
        $previous_configuration = $previous_line_item->getConfiguration();
      }
      else {
        // Use current payment and previous one.
        $previous_payment = $this->getPayment();
        /** @var \Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemInterface[] $previous_line_item */
        $previous_line_items = $previous_payment->getLineItems();
        $previous_line_item = reset($previous_line_items);
        $previous_configuration = $previous_line_item->getConfiguration();
      }

      list($period) = explode('|', $previous_configuration['periods']);
      $period = trim($period);
      $period = (int) $period;
      if ($this->isVerbose()) {
        $this->logger->error('Period: <pre>@data</pre>, Configuration: <pre>@configuration</pre>',
          [
            '@data' => print_r($period, TRUE),
            '@configuration' => print_r($configuration, TRUE),
          ]
        );
      }
      $timestamp = \Drupal::time()->getCurrentTime() + $period;
      $configuration['expiration_date'] = $timestamp;
      if ($this->isVerbose()) {
        $this->logger->error('Configuration: <pre>@data</pre>',
          [
            '@data' => print_r($configuration, TRUE),
          ]
        );
      }
      $line_item->setConfiguration($configuration);
    }
    $this->getPayment()->setLineItems($line_items);
    $this->getPayment()->save();

    /** @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $dispatcher */
    $dispatcher = \Drupal::service('event_dispatcher');
    // Create event object passing arguments.
    $event = new RobokassaPaymentRecurringEvent($this->getPayment(), $this->request);
    // Call it.
    $dispatcher->dispatch(RobokassaEvents::ROBOKASSA_PAYMENT_RECURRING_SUCCESS, $event);

    return [
      'status' => 'success',
      'message' => 'OK' . $this->getPayment()->id(),
      'response_code' => 200,
    ];
  }

  /**
   * Returns success message.
   *
   * @return array
   *   The renderable array.
   */
  public function getSuccessContent() {
    return [
      '#markup' => $this->t('Payment processed as @external_status.', array('@external_status' => 'success')),
    ];
  }

  /**
   * Returns fail message.
   *
   * @return array
   *   The renderable array.
   */
  public function getFailContent() {
    return [
      '#markup' => $this->t('Payment processed as @external_status.', array('@external_status' => 'FAIL')),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantIdName() {
    return 'MrchLogin';
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionIdName() {
    return 'InvId';
  }

  /**
   * {@inheritdoc}
   */
  public function getAmountName() {
    return 'OutSum';
  }

  /**
   * {@inheritdoc}
   */
  public function getSignatureName() {
    return 'SignatureValue';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    $required_keys = [
      'OutSum',
      'InvId',
    ];
    if (!$this->isFallbackMode()) {
      $required_keys[] = 'SignatureValue';
    }

    return $required_keys;
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigured() {
    return !empty($this->getAction());
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $elements = parent::buildConfigurationForm($form, $form_state);
    if ($this->isRecurring()) {
      $options = $this->getPeriods();
      if(count($options) > 1) {
        $elements['period'] = [
          '#type' => 'select',
          '#title' => $this->t('Period'),
          '#options' => $options,
        ];
      }
      else {
        $elements['period'] = [
          '#type' => 'value',
          '#value' => key($options),
        ];
        $elements['period_message'] = array(
          '#type' => 'markup',
          '#markup' => $this->t('Subscription period is %period', ['%period' => reset($options)]),
        );
      }
      $elements['recurring'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('I authorize regular payments at selected period.'),
        '#required' => TRUE,
      ];
    }

    return $elements;
  }

  /**
   * Requiring validator.
   *
   * TRUE if payment method is requiring FALSE otherwise.
   */
  public function isRecurring() {
    return $this->pluginDefinition["config"]["recurring"];
  }

  protected function getPeriods() {
    $options = [];
    $line_items = $this->getPayment()->getLineItemsByType('payment_robokassa_subscription');
    if (!empty($line_items)) {
      /** @var \Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemInterface $line_item */
      $line_item = reset($line_items);
      $periods = $line_item->getPeriods();
      $periods = explode("\n", $periods); // split lines into array
      $periods = array_filter($periods, 'strlen'); // remove empty lines
      foreach ($periods as $period) {
        list($seconds, $label) = explode('|', $period);
        $options[$seconds] = $label;
      }
    }
    return $options;
  }
}
