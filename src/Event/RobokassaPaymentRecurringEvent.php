<?php

namespace Drupal\robokassa_payment\Event;

use Drupal\payment\Entity\PaymentInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class RobokassaPaymentRecurringEvent extends Event {

  /**
   * @var \Drupal\payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * DummyFrontpageEvent constructor.
   */
  public function __construct(PaymentInterface $payment, Request $request ) {
    $this->payment = $payment;
    $this->request = $request;
  }

  /**
   * @return \Drupal\payment\Entity\PaymentInterface
   */
  public function getPayment() {
    return $this->payment;
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Request
   */
  public function getRequest() {
    return $this->request;
  }

}
