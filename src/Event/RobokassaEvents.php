<?php

namespace Drupal\robokassa_payment\Event;

/**
 * Defines Payment events.
 */
final class RobokassaEvents {

  /**
   * The name of the event that is fired when payment execution access is
   * checked.
   *
   * @see \Drupal\payment\Event\PaymentExecuteAccess
   */
  const ROBOKASSA_PAYMENT_RECURRING_SUCCESS = 'drupal.payment.robokassa_payment_recurring_access';

  /**
   * The name of the event that is fired before a payment is executed.
   *
   * @see \Drupal\payment\Event\PaymentPreExecute
   */
  const ROBOKASSA_PAYMENT_RECURRING_CANCEL = 'drupal.payment.robokassa_payment_recurring_cancel';

}
