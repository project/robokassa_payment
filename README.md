This module provide integration http://robokassa.ru payments for
https://www.drupal.org/project/payment (7.x-1.x version)

# Registering with Robokassa
Before you start the installation process you must register on robokassa.ru
and create your own Merchant. You will get "Merchant key", "Password 1" 
"Password 2"and other settings for your payment system.

# Installation and Configuration
Download the module from Drupal.org and extract it to your modules folder.
Enable it.
Go to /admin/config/services/payment/method/add and add Robokassa payment method.
Setup the settings according your data from Robokassa.

Additional information:

Default Process URL: http://yoursitename.com/robokassa/process
Default Success URL: http://yoursitename.com/robokassa/success
Default Fail URL: http://yoursitename.com/robokassa/fail

More API documentation can be found at Robokassa API
